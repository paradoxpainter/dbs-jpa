import connection.SetupServer;
import entity.BookEntity;
import entity.PersonEntity;
import repository.BookRepository;
import repository.ClientRepository;
import repository.PersonRepository;

import javax.persistence.*;
import java.io.IOException;
import java.util.function.Supplier;

public class Main {

    private static EntityManager EM =
            Persistence
                    .createEntityManagerFactory("NewPersistenceUnit")
                    .createEntityManager();

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        BookRepository bookRepository = new BookRepository(EM);
        PersonRepository personRepository = new PersonRepository(EM);

//        executeInTransaction(bookRepository::getAll);

        SetupServer server = new SetupServer();
        server.run();

//        System.out.println(executeInTransaction(bookRepository::getAll).get(0).getAuthors());
//        BookEntity book = new BookEntity();
//        book.setBookName("12312");
//        System.out.println(book.toString());
//        PersonEntity pers = new PersonEntity();
//
//
//
//        System.out.println(executeInTransaction(() -> {
//            personRepository.create(pers);
//            personRepository.update(pers);
//            return null;
//            return personRepository.getPswdByUsername("ttt");
//        }));

//        System.out.println(executeInTransaction(() -> {
////                BookEntity tmp = bookRepository.findOne(257);
//            book.setIsbn("FDSGHFJK-HFJKDH");
//            bookRepository.update(book);
//            System.out.println("book updated");
//            book.toString();
//            bookRepository.delete(book);
//            System.out.println("book deleted");
//            return book;
//        }));
//        System.out.println(executeInTransaction(() -> {
//                return bookRepository.findOne(262);
//            }));
//
//        executeInTransaction(() -> {
//                BookEntity tmp = bookRepository.findOne(261);
//                bookRepository.deleteById(261);
//                return null;
//            });
//        System.out.println(executeInTransaction(() -> {
//                return bookRepository.findOne(261);
//            }));

//        executeInTransaction(() -> {
//            BookEntity tmp = bookRepository.findOne(10);
//            return tmp.getAuthors();
//        });
    }


    public static <R> R executeInTransaction(Supplier<R> runnable) {
        EntityTransaction transaction = EM.getTransaction();
        transaction.begin();

        R result = runnable.get();

        transaction.commit();

        return result;
    }
//
//    public static void deleteBook(int id) {
//        EntityManager em = EMF.createEntityManager();
//        EntityTransaction et = null;
//        BookEntity book = null;
//
//        try {
//            et = em.getTransaction();
//            et.begin();
//            book = em.find(BookEntity.class, id);
//            em.remove(book);
//            em.persist(book);
//            et.commit();
//        } catch (Exception e) {
//            if (et != null) {
//                et.rollback();
//            }
//
//            e.printStackTrace();
//        } finally {
//            em.close();
//        }
//    }
//
//    public static void changeBookName(int id, String name) {
//        EntityManager em = EMF.createEntityManager();
//        EntityTransaction et = null;
//        BookEntity book = null;
//
//        try {
//            et = em.getTransaction();
//            et.begin();
//            book = em.find(BookEntity.class, id);
//            book.setBookName(name);
//            em.persist(book);
//            et.commit();
//        } catch (Exception e) {
//            if (et != null) {
//                et.rollback();
//            }
//
//            e.printStackTrace();
//        } finally {
//            em.close();
//        }
//
//    }
//
//    public static void getBook(int id) {
//        EntityManager em = EMF.createEntityManager();
//
//        String query = "SELECT b FROM BookEntity b WHERE b.bookId = :bookId";
//
//        TypedQuery<BookEntity> tq = em.createQuery(query, BookEntity.class);
//        tq.setParameter("bookId", id);
//        BookEntity book = null;
//
//        try {
//            book = tq.getSingleResult();
//            System.out.println(book.getBookName() + " " + book.getIsbn());
//
//        } catch (NoResultException e) {
//            e.printStackTrace();
//        } finally {
//            em.close();
//        }
//    }
//
//    public static void getBooks() {
//        EntityManager em = EMF.createEntityManager();
//        String query = "SELECT b FROM BookEntity b WHERE b.publishDt IS NOT NULL";
//        TypedQuery<BookEntity> tq = em.createQuery(query, BookEntity.class);
//        List<BookEntity> books = null;
//
//        try {
//            books = tq.getResultList();
//            books.forEach(book -> System.out.println(book.getBookName() + " " + book.getIsbn()));
//        } catch (NoResultException e) {
//            e.printStackTrace();
//        } finally {
//            em.close();
//        }
//    }
}

package repository;

import entity.ClientEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ClientRepository extends CRUDRepository<ClientEntity> {
    public ClientRepository(EntityManager transactionManager) {
        super(transactionManager, ClientEntity.class);
    }
}

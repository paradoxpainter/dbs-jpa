package repository;

import entity.PersonEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonRepository extends CRUDRepository<PersonEntity> {
    public PersonRepository(EntityManager transactionManager) {
        super(transactionManager, PersonEntity.class);
    }

    public boolean isLoginDataCorrect(String username, String password) {
        String check = getPswdByUsername(username);

        if (check.equals(password)) {
            return true;
        }

        return false;
    }

    public String getPswdByUsername(String username) {
        TypedQuery<PersonEntity> query = transactionManager.createQuery(
                "SELECT p FROM PersonEntity p WHERE (p.username = :username)",
                clazz
        );
        query.setParameter("username", username);
        List<PersonEntity> list = query.getResultList();

        if (list.isEmpty()) {
            return null;
        }

        return list.get(0).getPasswd();
    }

    public int getPersonIdByUsername(String username) {
        TypedQuery<PersonEntity> query = transactionManager.createQuery(
                "SELECT p FROM PersonEntity p WHERE (p.username = :username)",
                clazz
        );
        query.setParameter("username", username);
        List<PersonEntity> list = query.getResultList();

        if (list.isEmpty()) {
            return -1;
        }

        return list.get(0).getPersonId();
    }
}

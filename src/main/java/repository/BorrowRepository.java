package repository;

import entity.BorrowedBookEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class BorrowRepository extends CRUDRepository<BorrowedBookEntity> {
    public BorrowRepository(EntityManager transactionManager) {
        super(transactionManager, BorrowedBookEntity.class);
    }

    public Integer borrowBook(Integer clientId, Integer bookId) {
        transactionManager.createQuery(
                "INSERT INTO BorrowedBookEntity b (b.bookId, b.clientId, b.borrowDt, b.returnDt) VALUES (?, ?, ?, ?)"
        )
                .setParameter(1, bookId)
                .setParameter(2, clientId)
                .setParameter(3, LocalDate.now())
                .setParameter(4, LocalDate.now().plusDays(14))
                .executeUpdate();

        return bookId;
    }

    public BorrowedBookEntity getBorrowEntity(int clientId, int bookId) {
        TypedQuery<BorrowedBookEntity> query = transactionManager.createQuery(
                "SELECT b " +
                        "FROM BorrowedBookEntity b " +
                        "WHERE (b.bookId = :bookId AND b.clientId = :clientId AND (b.returnDt IS NULL))",
                clazz
        );
        query.setParameter("bookId", bookId);
        query.setParameter("clientId", clientId);
        List<BorrowedBookEntity> list = query.getResultList();

        if (list.isEmpty()) {
            return null;
        }

        return list.get(0);
    }
}

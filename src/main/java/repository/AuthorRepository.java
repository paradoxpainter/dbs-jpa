package repository;

import entity.AuthorEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;

public class AuthorRepository extends CRUDRepository<AuthorEntity> {
    public AuthorRepository(EntityManager transactionManager, Class<AuthorEntity> clazz) {
        super(transactionManager, clazz);
    }
}

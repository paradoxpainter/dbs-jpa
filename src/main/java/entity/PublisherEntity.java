package entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "publisher", schema = "public", catalog = "db19_shcheden")
public class PublisherEntity {
    private int publisherId;
    private Integer addressId;
    private String companyName;
    private Collection<BookEntity> booksByPublisherId;
    private AddressEntity addressByAddressId;

    @Id
    @Column(name = "publisher_id", nullable = false)
    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }

    @Basic
    @Column(name = "address_id", nullable = true)
    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "company_name", nullable = false, length = 50)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PublisherEntity that = (PublisherEntity) o;
        return publisherId == that.publisherId &&
                Objects.equals(addressId, that.addressId) &&
                Objects.equals(companyName, that.companyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(publisherId, addressId, companyName);
    }

    @OneToMany(mappedBy = "publisherByPublisherId")
    public Collection<BookEntity> getBooksByPublisherId() {
        return booksByPublisherId;
    }

    public void setBooksByPublisherId(Collection<BookEntity> booksByPublisherId) {
        this.booksByPublisherId = booksByPublisherId;
    }

    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName = "address_id", insertable = false, updatable = false)
    public AddressEntity getAddressByAddressId() {
        return addressByAddressId;
    }

    public void setAddressByAddressId(AddressEntity addressByAddressId) {
        this.addressByAddressId = addressByAddressId;
    }
}

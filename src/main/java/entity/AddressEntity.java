package entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "address", schema = "public", catalog = "db19_shcheden")
public class AddressEntity {
    private int addressId;
    private String postcode;
    private String country;
    private String city;
    private String street;
    private String building;
    private Integer apartment;
//    private Collection<ClientAddressEntity> clientAddressesByAddressId;
    private Collection<PublisherEntity> publishersByAddressId;

    @Id
    @Column(name = "address_id", nullable = false)
    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "postcode", nullable = true, length = 10)
    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 50)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 50)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "street", nullable = true, length = 50)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "building", nullable = true, length = 10)
    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    @Basic
    @Column(name = "apartment", nullable = true)
    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddressEntity that = (AddressEntity) o;
        return addressId == that.addressId &&
                Objects.equals(postcode, that.postcode) &&
                Objects.equals(country, that.country) &&
                Objects.equals(city, that.city) &&
                Objects.equals(street, that.street) &&
                Objects.equals(building, that.building) &&
                Objects.equals(apartment, that.apartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressId, postcode, country, city, street, building, apartment);
    }

//    @OneToMany(mappedBy = "addressByAddressId")
//    public Collection<ClientAddressEntity> getClientAddressesByAddressId() {
//        return clientAddressesByAddressId;
//    }
//
//    public void setClientAddressesByAddressId(Collection<ClientAddressEntity> clientAddressesByAddressId) {
//        this.clientAddressesByAddressId = clientAddressesByAddressId;
//    }

    @OneToMany(mappedBy = "addressByAddressId")
    public Collection<PublisherEntity> getPublishersByAddressId() {
        return publishersByAddressId;
    }

    public void setPublishersByAddressId(Collection<PublisherEntity> publishersByAddressId) {
        this.publishersByAddressId = publishersByAddressId;
    }
}
